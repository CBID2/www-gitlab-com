---
features:
  secondary:
  - name: "Updated SAST rules to reduce false-positive results"
    available_in: [ultimate]
    documentation_link: 'https://docs.gitlab.com/ee/user/application_security/sast/rules.html#important-rule-changes'
    reporter: connorgilbert
    stage: secure
    categories:
    - SAST
    epic_url: 'https://gitlab.com/groups/gitlab-org/-/epics/8170'
    description: |
      We've updated the default ruleset used in GitLab SAST to provide higher-quality results.
      We analyzed each rule that was previously included by default, then removed rules that did not provide enough value in most codebases.

      The rule changes are included in updated versions of the Semgrep-based GitLab SAST [analyzer](https://docs.gitlab.com/ee/user/application_security/sast/analyzers.html).
      This update is automatically applied on GitLab 16.0 or newer unless you've [pinned SAST analyzers to a specific version](https://docs.gitlab.com/ee/user/application_security/sast/#pinning-to-minor-image-version).
      
      Existing scan results from the removed rules are [automatically resolved](https://docs.gitlab.com/ee/user/application_security/sast/#automatic-vulnerability-resolution) after your pipeline runs a scan with the updated analyzer.

      We're working on more SAST rule improvements in [epic 10907](https://gitlab.com/groups/gitlab-org/-/epics/10907).
