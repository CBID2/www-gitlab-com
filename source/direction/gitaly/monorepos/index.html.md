---
layout: handbook-page-toc
title: "Product Direction - Monorepos"
description: "GitLab want to improve the monorepo experience. Learn more here!"
---

- TOC
{:toc}

## Overview

Monolithic repositories, more commonly known as monorepos, are a software
development approach where code for many projects is stored in a single
repository.

Monorepos provide a number of advantages such as reduced complexity, code reuse,
easier collaboration amongst teams, and streamlined dependency management.
Additionally, there are [several drawbacks](https://medium.com/@mattklein123/monorepos-please-dont-e9a279be011b)
such as difficult to scale, performance concerns, requires large amounts of
storage, and loss of per-service semantic versioning.

## Existing monorepo experience inside of GitLab

We want to improve the monorepo experience - whether it has multiple services
that all get deployed together, or if it's a mobile app in a single repo that
needs to be built completely differently for iOS and Android. 

The primary stages that will be supporting our development of a better monorepo
experience are:

- [Gitaly](/direction/gitaly)
- [Create](/direction/dev/#create-1)
- [Verify](/direction/verify/)
- [Secure::SAST](/direction/secure/static-analysis/sast/)

Secondary stages contributing to the betterment of monorepos in GitLab are:

- [Package](/direction/ops/#package)
- [Configure](https://about.gitlab.com/direction/ops/#configure)

GitLab already has a number of important features that help monorepos:

- [Git LFS](https://docs.gitlab.com/ee/topics/git/lfs/index.html#git-lfs)
- [Parent-child pipelines](https://docs.gitlab.com/ee/ci/parent_child_pipelines.html)
- [Dynamic pipeline creation for artifact includes](https://docs.gitlab.com/ee/ci/parent_child_pipelines.html#dynamic-child-pipelines)
- [Monorepo package management](https://docs.gitlab.com/ee/user/packages/npm_registry/index.html)

## What's next & why

The primary way for us to improve the monorepo experience is by focusing on
scale & performance.

### [Bundle URIs](https://gitlab.com/groups/gitlab-org/-/epics/8939)

Bundle URIs is a [Git feature](https://git-scm.com/docs/bundle-uri) whereby a
clone or fetch can be served through an http link to a bundle. This has the
distinct advantage of saving the server from having to spin up a
`git-pack-objects` process to pack up the objects into a packfile to then send
over the wire.

### [Next generation reftables backend in Git](https://gitlab.com/groups/gitlab-org/-/epics/4220)

Currently in Git references are represented on disk as either separate files per
reference, or a packed-refs file that contains multiple references. This
implementation can be problematic in repositories with a huge number of
references. A new implementation of references in Git where a binary data
structure is used instead can greatly improve performance of both reading and
writing references.

This was proposed and worked on on the Git mailing list, but it hasn't yet been
upstreamed into the Git project. The Gitaly team will be doing the work to
upstream this feature into Git itself. This can have a vast improve performance
in monorepos.

### Competitive landscape 

GitHub has released a [Remote Repository Plug-in](https://marketplace.visualstudio.com/items?itemName=github.remotehub)
to better meet the developer needs around monorepo experiences. This extension
enables users to quickly commit to GitHub directly from Visual Studio. This
empowers users to easily navigate some of the git complexities that come with
monorepos.

GitHub and Perforce recently announced a [strategic partnership](https://developer.microsoft.com/en-us/games/blog/microsoft-perforce-reimagining-game-production/)
to combat the limitations of Git when it comes to developing on large mono
repositories. Perforce authored a set of GitHub Actions that allow the user to
spin up a Perforce Helix Core server when working on repositories that would
otherwise not be performant in GitHub alone. 

