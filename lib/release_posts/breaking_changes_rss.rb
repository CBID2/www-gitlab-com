# frozen_string_literal: true

require 'gitlab'
require 'nokogiri'
require_relative '../api_retry'

module ReleasePosts
  class BreakingChangesRSS
    include ApiRetry
    include Helpers

    BREAKING_CHANGES_FILE = Pathname.new(__dir__).join('..', '..', 'public', 'breaking-changes.xml').expand_path

    def create
      log_info("Creating #{BREAKING_CHANGES_FILE}...")
      builder = Nokogiri::XML::Builder.new(encoding: 'UTF-8') do |xml|
        xml.rss('version' => '2.0') do
          xml.channel do
            xml.title("GitLab Breaking Changes")
            xml.link("https://docs.gitlab.com/ee/update/deprecations.html")
            xml.description("Receive notifications of upcoming changes to GitLab that may affect your team's workflow.")
            ReleasePosts::Deprecations.new.retrieve_deprecations(reverse_sort_by: :first_merged).each do |change|
              xml.item do
                xml.title(change.title)
                xml.link(change.link)
                xml.description_ do
                  xml.cdata(format_description(change.description))
                end
                xml.pubDate(change.date)
              end
            end
          end
        end
      end

      BREAKING_CHANGES_FILE.dirname.mkpath
      BREAKING_CHANGES_FILE.write(builder.to_xml)
    end

    private

    def format_description(description)
      description
        .gsub /\[([^\]]+)\]\(([^)]+)\)/, '<a href="\2">\1</a>'
        .delete("\n")
    end
  end
end
